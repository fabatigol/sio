<h3>Fiche de frais du mois <?php echo $numMois . "-" . $numAnnee ?> : 
</h3>
<div class="encadre">
    <p>
        Etat : <?php echo $libEtat ?> depuis le <?php echo $dateModif ?> <br> Montant validé : <?php echo $montantValide ?>
    </p>
    <table class="listeLegere">
        <caption>Eléments forfaitisés </caption>
        <tr>
            <?php
            foreach ($lesFraisForfait as $unFraisForfait) {
                $libelle = $unFraisForfait['libelle'];
                ?>	
                <th> <?php echo $libelle ?></th>
                <?php
            }
            ?>
        </tr>
        <tr>
            <?php
            foreach ($lesFraisForfait as $unFraisForfait) {
                $quantite = $unFraisForfait['quantite'];
                ?>
                <td class="qteForfait"><?php echo $quantite ?> </td>
                <?php
            }
            ?>
        </tr>
    </table>
    <table class="listeLegere">
        <caption>Descriptif des éléments hors forfait -<?php echo $nbJustificatifs ?> justificatifs reçus -
        </caption>
        <tr>
            <th class="date">Date</th>
            <th class="libelle">Libellé</th>
            <th class='montant'>Montant</th>                
        </tr>
        <?php
        foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
            $date = $unFraisHorsForfait['date'];
            $libelle = $unFraisHorsForfait['libelle'];
            $montant = $unFraisHorsForfait['montant'];
            ?>
            <tr>
                <td><?php echo $date ?></td>
                <td><?php echo $libelle ?></td>
                <td><?php echo $montant ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
    if ($libEtat == "Validée") {
        ?>
        <form method="POST"  action="index.php?uc=suivreFrais&action=miseEnPaiement">
            <input type="hidden" id="mois" name="hdmois" value="<?php echo $unMois; ?>" >
            <input type="hidden" id="idVisiteur" name="hdvisiteur" value="<?php echo $unVisiteur; ?>" >
            <div class="piedForm">
                <p>
                    <input id="ok" type="submit" value="Mise en Paiement" size="20" />
                </p> 
            </div>
            <?php
        }
        if ($libEtat == "Mise en Paiement") {
            ?>
            <form method="POST"  action="index.php?uc=suivreFrais&action=rembourse">
                <input type="hidden" id="mois" name="hdmois" value="<?php echo $unMois; ?>" >
                <input type="hidden" id="idVisiteur" name="hdvisiteur" value="<?php echo $unVisiteur; ?>" >
                <div class="piedForm">
                    <p>
                        <input id="ok" type="submit" value="Remboursée" size="20" />
                    </p> 
                </div>
            <?php }
            ?>
        </form>
</div>
</div>














