<?php

include("../include/class.pdogsbForTest.php");
include("../include/fct.inc.php");

class PdoGsbTest extends PHPUnit_Extensions_Database_TestCase {

    // only instantiate pdo once for test clean-up/fixture load
    static private $pdo = null;
    // only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
    private $conn = null;

    //connection à la base test
    final public function getConnection() {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    /*
     * Récupération du jeu de données
     */
    public function getDataSet() {
        return $this->createMySQLXMLDataSet('xmlFixtureFast.xml');
    }

    public function testGetNombreEtat(){
    $this->assertEquals(5, $this->getConnection()->getRowCount('etat'));
    }
    
    /*
     * Test de la méthode GetLesInfosFicheFrais
     */
    public function testGetLesInfosFicheFrais() {
       $query = $this->getConnection()->createQueryTable("fichefrais", "select fichefrais.idEtat as idEtat, fichefrais.dateModif as dateModif, fichefrais.nbJustificatifs as nbJustificatifs, fichefrais.montantValide as montantValide, etat.libelle as libEtat from  fichefrais join Etat on fichefrais.idEtat = Etat.id where fichefrais.idvisiteur='a131' and fichefrais.mois='201609'");
       $resultatAttendu = $this->createXmlDataset('infoFraisOk.xml')->getTable('fichefrais');
       $this->assertTablesEqual($resultatAttendu, $query);
    }

    /*
     * Test de la méthode comptabiliserLesFraisHorsForfait
     */
    public function testComptabiliserLesFraisHorsForfait() {
        $visiteur = "a131";
        $mois = "201609";
        $pdo = PdoGsb::getPdoGsb();
        $res = $pdo->comptabiliserLesFraisHorsForfait($visiteur, $mois);
        $this->assertEquals(1386.00, $res);
    }

    /*
     * Test de la méthode getFicheAValider
     */
    public function testGetFicheAValider() {
        $query1 = $this->getConnection()->createQueryTable("fichefrais", "select * from  fichefrais where fichefrais.idvisiteur='a131' and fichefrais.idetat='VA'");
        $resultatAttendu = $this->createXmlDataset('ficheAValider.xml')->getTable('fichefrais');
        $this->assertTablesEqual($resultatAttendu, $query1);
    }
    
    /*
     * Test de la méthode majLibelleRefuse
     */
    public function testMajLibelleRefuse() {  
        $idFrais = 261;
        $pdo = PdoGsb::getPdoGsb();
        $test = $pdo->majLibelleRefuse($idFrais);
        $query = $this->getConnection()->createQueryTable("lignefraishorsforfait", "select libelle as libelle from lignefraishorsforfait where id = '$idFrais'");
        $resultatAttendu = $this->createXmlDataset('libelleRefuse.xml')->getTable("lignefraishorsforfait");
        $this->assertTablesEqual($resultatAttendu, $query);
    }   
    
    /*
     * Test de la méthode supprimerFraisHorsFofait
     * Suppresion d'un frais ici le numéro 237
     * Lancement de la méthode de suppression
     * Vérification si le nombre d'enregistrement est égale à 29 soit un de moins avant suppression
     */
    public function testSupprimerFraisHorsForfait(){
        $idFrais = 237;   
        $pdo =  PdoGsb::getPdoGsb();
        $suppression = $pdo->SupprimerFraisHorsForfait($idFrais);
        $this->assertEquals(29, $this->getConnection()->getRowCount('lignefraishorsforfait'));        
     }
     
     
    /*
     * Test de la méthode getInfosVisiteur
     */
     
    public function testGetInfosVisiteur() {
        $query = $this->getConnection()->createQueryTable("visiteur", "select login as login, mdp as mdp from visiteur where login='lvillachane'");
        $resultatAttendu = $this->createXmlDataset('infoVisiteurOk.xml')->getTable("visiteur");
        $this->assertTablesEqual($resultatAttendu,$query);
    }
    
    
    /*
     * Test de la méthode majNbJustificatifs
     */
    public function testMajNbJustificatifs() {
        $idVisiteur = "a131";
        $mois = "201609";
        $nbJustificatifs = 37;
        
        $pdo = PdoGsb::getPdoGsb();
        $test = $pdo->MajNbJustificatifs($idVisiteur,$mois,$nbJustificatifs);
        $query = $this->getConnection()->createQueryTable("fichefrais", "select nbJustificatifs as nbJustificatifs from fichefrais where idVisiteur = 'a131' and mois='201609'");
        $resultatAttendu = $this->createXmlDataset('nbJustificatif.xml')->getTable("fichefrais");
        $this->assertTablesEqual($resultatAttendu, $query);
        
    }
    
    }
