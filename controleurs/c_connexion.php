﻿<?php
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'demandeConnexion';
}
$action = $_REQUEST['action'];
switch ($action) {
    case 'demandeConnexion': {
            include("vues/v_connexion.php");
            break;
        }
    case 'valideConnexion': {
            $login = $_REQUEST['login'];
            $mdp = $_REQUEST['mdp'];
            $visiteur = $pdo->getInfosVisiteur($login, $mdp);
            if (!is_array($visiteur)) {
                ajouterErreur("Login ou mot de passe incorrect");
                include("vues/v_erreurs.php");
                include("vues/v_connexion.php");
            } else {
                $id = $visiteur['id'];
                $nom = $visiteur['nom'];
                $prenom = $visiteur['prenom'];
                $profil = $visiteur['profil'];
                connecter($id, $nom, $prenom);
                // Si le profil est celui d'un comptable, le sommaire sera différent
                if ($profil == '1') {
                    include("vues/v_sommaireComptable.php");
                } else {
                    include("vues/v_sommaire.php");
                }
            }
            break;
        }
        case 'deconnexion': {
                deconnecter();
                include("vues/v_connexion.php");
        }break;
    default : {
            include("vues/v_connexion.php");
            break;
        }
}
?>
