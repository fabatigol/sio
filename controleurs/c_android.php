<?php
require_once("../include/fct.inc.php");
require_once ("../include/class.pdogsb.inc.php");

// Vérification de la présence de données de type Post
if (isset($_REQUEST['android'])){
    
    // Vérification de l'opération Android
    if ($_REQUEST['android']=="connexion"){
        try{
            $pdo = PdoGsb::getPdoGsb();
            // récupération des données en post
	    $lesdonnees = $_REQUEST["lesDonnees"] ;
	    $donnee = json_decode($lesdonnees) ;
	    $login = $donnee[0] ;
	    $mdp = $donnee[1] ;
              
            // s'il y a un profil, on récupère l'id sinon on retourne null
	    $id = $pdo->getIdVisiteur($login, $mdp);
            print("connexion%".json_encode($id)) ;

        }catch (PDOException $e){
            print "Erreur !" . $e->getMessage();
            die();
        }
        // enregistrement dans la table profil du profil reçu
    }elseif ($_REQUEST["android"]=="enregistrer") {
        // récupération des données en post
	$lesdonnees = $_REQUEST["lesDonnees"] ;
	$donnee = json_decode($lesdonnees, TRUE) ;
        $id = $donnee[0];
        $key = $donnee[1];
        $nbligne = count($donnee);

        //Création du tableau associatif de frais forfait
        $lesFrais = array(
            "KM" => $donnee[2],
            "ETP" => $donnee[3],
            "NUI" => $donnee[4],
            "REP" => $donnee[5],
        );
 
        //exécution des réquetes d'update dans la base
        try {
            $pdo = PdoGsb::getPdoGsb();
            //Vérification de la présence du mois dans la base par rapport au visiteur
            //création du frais si celui-ci n'existe pas
            $pdo->creeNouvellesLignesFrais($id, $key);
            $fraisForfait = $pdo->majFraisForfait($id, $key, $lesFrais);
            
            //suppression de tous les frais hors forfait avant insertion
            //afin d'éviter les doublons et gérer de manière simple
            //la suppression d'un frais depuis l'application android
            $pdo->supprimerTousLesFraisHorsForfait($id, $key);
            //Récupération des frais hors forfait et enregistrement en base
            for ($i = 7; $i<=$nbligne; $i+=3){
                $j =$i;
                $montant = $donnee[$j];
                $j++;
                $motif =$donnee[$j];
                $j++;
                $jour = $donnee[$j];
                $date = reconstituerDate($key, $jour);
                $FraisHorsForfait = $pdo->creeNouveauFraisHorsForfait($id, $key, $motif, $date, $montant);
            }
           
        print("enregistrer%".json_encode($fraisForfait));
        // capture d'erreur d'accès à la base de données
        } catch (PDOException $e) {
            print "Erreur !" . $e->getMessage();
            die();
        }
        
    }
}